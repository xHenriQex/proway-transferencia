let menu = document.getElementById("div-dropdown");

document.getElementById("user-menu").onclick = function () {
    menu.style = "visibility:visible;transition:1s;padding-left:25px;padding-right:40px;";
}

document.getElementById("fechar").onclick = function () {
    menu.style = "visibility:hidden;transition:0.2s;";
}


//Função que emite um alerta confirmando transferência
function confirmarTransfer() {
    alert("Transferência realizada com sucesso");
    window.location.href="confirmar.html";
}


//Função que checa se todos os dados do formulário está preenchido
function checarFormulario() {
    let dados = document.getElementsByClassName('required');
    let contador = dados.length;
    let valid = true;
    for (let i = 0; i < contador; i++) {
        console.log(dados[i])
        if (!dados[i].value){ 
            valid = false; 
        }
    }
    if (!valid) {
        alert('Por favor preencha todos os campos.');
        return false;
    }else{ 
       //return true;
       window.open("confirmacao-transferencia.html");
    }
    
}
