let menu = document.getElementById("div-dropdown");

document.getElementById("user-menu").onclick = function(){
    menu.style.display = "block";
}

document.getElementById("fechar").onclick = function(){
    menu.style.display = "none";
}

let caixaTexto = document.getElementById("caixaDeTexto")
const trocaTexto = () => {
    let aux = event.target;
    caixaTexto.value = "";
    caixaTexto.type="text"
    caixaTexto.removeAttribute('disabled');
    switch(aux.innerHTML){
        case "CPF":
            caixaTexto.placeholder="CPF"
            break;
        case "E-mail":
            caixaTexto.placeholder="seuemail@hotmail.com" 
            caixaTexto.type="email"
            break;
        case "Telefone":
            caixaTexto.placeholder="(XX) XXXXX-XXXX"
            caixaTexto.type="tel"
            break;
        case "Chave Aleatória":
            caixaTexto.placeholder="Chave Aleatória"
            break;
    }
}

let Bnt = document.getElementsByClassName("botoes")

for (let i = 0; i <= Bnt.length; i++){
    Bnt[i].addEventListener("click", trocaTexto);
}




