var menu = document.getElementById("div-dropdown");
var usericon = document.getElementById("user-menu");
var faixapix = document.getElementById("faixa-pix");
var logo = document.getElementById("logo");
var fechar = document.getElementById("fechar");
var optDesk = document.getElementById("opt-desktop");


usericon.onmouseover = function(){
    menu.style = "visibility:visible;padding-left:25px;padding-right:40px;";
}

usericon.onclick = function(){
    menu.style = "visibility:visible;transition:1s;padding-left:25px;padding-right:40px;";
}

menu.onmouseleave = function(){
    menu.style = "visibility:hidden;";
}

// faixapix.onmouseover = function(){
//     menu.style = "visibility:hidden;";
// }

// logo.onmouseover = function(){
//     menu.style = "visibility:hidden;";
// }

fechar.onclick = function(){
    menu.style = "visibility:hidden;";
}



//Listener para os botões de navegação
addGlobalEventListener("click", "button.botaoNavegacao", e => {
    let btnId = e.target.id;
    
    //Colocar os nomes corretos das páginas que cada botão irá direcionar
    switch(btnId) {
        case "btnPix":
            navegar("pix-inicial");
            break;
        case "btnComprovantes":
            navegar("meus-comprovantes");
            break;    
        case "btnTransferencia":
            navegar("transferencia-ted-doc");
            break;
        case "btnContatos":
            navegar("contatos-pix");
            break;
    }
})

//Listenr para o botão de voltar
addGlobalEventListener("click", "button.botaoVoltar", () => {
    //Adicionar no parâmetro a navegação para a tela principal da aplicação (homepage do internet banking)
    navegar("transferencia-index");
});

//Listener para os botões de imprimir comprovante
//TODO implementar lógica que irá chamar as APIS necessárias para de fato realizar tais ações
addGlobalEventListener("click", "i.imprimir", (e) => {
    let id = e.target.id;

    switch(id) {
        case "imprimirImpressora":
            exibirAlerta("Comprovante impresso com sucesso!");
            break;
        case "imprimirPdf":
            exibirAlerta("PDF salvo com sucesso!");
            break;
        case "imprimirDrive":
            exibirAlerta("Comprovante salvo no Google Drive com sucesso!");
            break;
        case "imprimirDropbox":
            exibirAlerta("Comprovante salvo no Dropbox com sucesso!");
            break;
    }
})

addGlobalEventListener("click", "i.compartilhar", (e) => {
    let id = e.target.id;

    switch(id) {
        case "compartilharEmail":
            exibirAlerta("Comprovante enviado por e-mail com sucesso!");
            break;
        case "compartilharWhatsapp":
            exibirAlerta("Comprovante enviado por WhatsApp com sucesso!");
            break;
        case "compartilharTelegram":
            exibirAlerta("Comprovante enviado pelo Telegram com sucesso!");
            break;
    }
})


const navegar = (paginaDestino) => {
    // Colocar como destino a página que for passada como parâmetro da função
    //Implementar alguma lógica para o caso de a página estar em outra pasta
    window.location.href= `${paginaDestino}.html`;
}

//Função para exibir um alert na página, mostrando uma mensagem
const exibirAlerta = (msg) => {
    alert(msg);
}

//Função para adicionar um listener em uma tag ou class específica
function addGlobalEventListener(type, selector, callback) {
    document.addEventListener(type, evt => {
        if (evt.target.matches(selector)) {
            callback(evt);
        }
    })
}

